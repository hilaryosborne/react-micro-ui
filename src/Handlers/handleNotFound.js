const handleNotFound = async () => ({
  status: 404,
});

export default handleNotFound;
