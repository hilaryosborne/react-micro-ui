"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "renderComponent", {
  enumerable: true,
  get: function () {
    return _renderComponent.default;
  }
});
Object.defineProperty(exports, "childComponent", {
  enumerable: true,
  get: function () {
    return _childComponent.default;
  }
});
Object.defineProperty(exports, "hydrateComponent", {
  enumerable: true,
  get: function () {
    return _hydrateComponent.default;
  }
});

var _renderComponent = _interopRequireDefault(require("./renderComponent"));

var _childComponent = _interopRequireDefault(require("./childComponent"));

var _hydrateComponent = _interopRequireDefault(require("./hydrateComponent"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }