"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

const handleNotFound = async () => ({
  status: 404
});

var _default = handleNotFound;
exports.default = _default;